#!/usr/bin/env python
# coding:utf-8

from datetime import datetime
import peewee
from playhouse.postgres_ext import *
from connections import db_mysql


class BondEarning(peewee.Model):
    """obj mapping to table: bond_earnings_rate in mysqlDB"""
    pk = peewee.IntegerField(
        primary_key=True,
    )
    bond_type     = peewee.CharField(max_length=50, null=False)
    maturity      = peewee.DateField(unique=True, default=datetime.now)
    earnings__rate= peewee.CharField(null=False)

    class Meta:
        database = db_mysql
        schema = 'test'
        db_table = 'bond_earnings_rate'

    def save(self, force_insert=False, only=None):
        try:
            super(BondEarning, self).save(force_insert=True, only=only)
        except peewee.DatabaseError as e:
            print(e)
            logger.exception('BondEarning')
            db_mysql.rollback()
            return None

