# -*- coding: utf-8 -*-
# Keywords: Mysql, bond_earnings_rate
# Usage: earnings rate data to mysqlDB.bond_earnings_rate
# Commearning:

import os
import logging
from openpyxl import load_workbook
import sys
BASE_DIR = os.getcwd()
print 'base dir..........', BASE_DIR
from models import BondEarning
from datetime import datetime

# datestr = datetime.strftime(datetime.now(), '%Y-%m-%d_%H')
# logger = logging.getLogger('bond')
# hdlr = logging.FileHandler(BASE_DIR + '/logs/earning_%s.log' % datestr)
# formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
# hdlr.setFormatter(formatter)
# logger.addHandler(hdlr)
# logger.setLevel(logging.INFO)


if __name__ == '__main__':
    # logger.info('%s start...' % os.path.basename(__file__))
    print(u'开始...')
    f = os.getcwd()
    fl = os.listdir(f)
    bond = BondEarning()
    title_list = []
    for f in fl:
        if '.xlsx' in f:
            # # save formula
            # wb = load_workbook(f, read_only=True)
            # save value
            wb = load_workbook(f, data_only=True)
            sheets = wb.get_sheet_names()
            # 2 tabs
            for sheet_num in range(0,2):
                bond.bond_type= sheets[sheet_num]
                print bond.bond_type
                ws = wb[sheets[sheet_num]]
                # 第一行标题
                for row, tt in enumerate(ws.iter_rows()):
                    if row ==0:
                        for cell in tt:
                            title_list.append(cell.value)
                        break
                for row, rates in enumerate(ws.iter_rows()):
                    if  row >0:
                        rate_dict = {}
                        bond.maturity = rates[0].value
                        for col, cell in enumerate(rates):
                            if col > 0:
                                yr = title_list[col]
                                rate = cell.value
                                rate_dict.update({yr: rate})
                        bond.earnings__rate = str(rate_dict)
                        bond.save()

""" bond_earnings_rate
    maturity            到期日, 2016-03-31
    earnings_rate       收益率, {'1年':0.025}
    bond_type           债券类型, 国债

"""
